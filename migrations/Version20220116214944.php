<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220116214944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE apartment (id INT AUTO_INCREMENT NOT NULL, building_id INT NOT NULL, apartment_type VARCHAR(255) NOT NULL, area INT NOT NULL, price INT NOT NULL, INDEX IDX_4D7E68544D2A7E12 (building_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE apartment_invoice (id INT AUTO_INCREMENT NOT NULL, apartment_id INT NOT NULL, year INT NOT NULL, UNIQUE INDEX UNIQ_1C53F9CE176DFE85 (apartment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE building (id INT AUTO_INCREMENT NOT NULL, project_id INT NOT NULL, year INT NOT NULL, INDEX IDX_E16F61D4166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE building_project (id INT AUTO_INCREMENT NOT NULL, building_type VARCHAR(255) NOT NULL, one_bedroom_qty INT NOT NULL, two_bedroom_qty INT NOT NULL, three_bedroom_qty INT NOT NULL, steel_qty INT NOT NULL, concrete_qty INT NOT NULL, bricks_qty INT NOT NULL, min_lot_area INT NOT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE demand (id INT AUTO_INCREMENT NOT NULL, apartment_type VARCHAR(255) NOT NULL, building_type VARCHAR(255) NOT NULL, apartment_qty INT NOT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lot_invoice (id INT AUTO_INCREMENT NOT NULL, total INT NOT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE material (id INT AUTO_INCREMENT NOT NULL, material_type VARCHAR(255) NOT NULL, price INT NOT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE material_invoice (id INT AUTO_INCREMENT NOT NULL, total INT NOT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE apartment ADD CONSTRAINT FK_4D7E68544D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id)');
        $this->addSql('ALTER TABLE apartment_invoice ADD CONSTRAINT FK_1C53F9CE176DFE85 FOREIGN KEY (apartment_id) REFERENCES apartment (id)');
        $this->addSql('ALTER TABLE building ADD CONSTRAINT FK_E16F61D4166D1F9C FOREIGN KEY (project_id) REFERENCES building_project (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE apartment_invoice DROP FOREIGN KEY FK_1C53F9CE176DFE85');
        $this->addSql('ALTER TABLE apartment DROP FOREIGN KEY FK_4D7E68544D2A7E12');
        $this->addSql('ALTER TABLE building DROP FOREIGN KEY FK_E16F61D4166D1F9C');
        $this->addSql('DROP TABLE apartment');
        $this->addSql('DROP TABLE apartment_invoice');
        $this->addSql('DROP TABLE building');
        $this->addSql('DROP TABLE building_project');
        $this->addSql('DROP TABLE demand');
        $this->addSql('DROP TABLE lot_invoice');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE material_invoice');
    }
}
