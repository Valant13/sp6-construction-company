<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220117183601 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lot_invoice ADD building_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lot_invoice ADD CONSTRAINT FK_3DE551674D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3DE551674D2A7E12 ON lot_invoice (building_id)');
        $this->addSql('ALTER TABLE material_invoice ADD building_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE material_invoice ADD CONSTRAINT FK_21FFCCEB4D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id)');
        $this->addSql('CREATE INDEX IDX_21FFCCEB4D2A7E12 ON material_invoice (building_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lot_invoice DROP FOREIGN KEY FK_3DE551674D2A7E12');
        $this->addSql('DROP INDEX UNIQ_3DE551674D2A7E12 ON lot_invoice');
        $this->addSql('ALTER TABLE lot_invoice DROP building_id');
        $this->addSql('ALTER TABLE material_invoice DROP FOREIGN KEY FK_21FFCCEB4D2A7E12');
        $this->addSql('DROP INDEX IDX_21FFCCEB4D2A7E12 ON material_invoice');
        $this->addSql('ALTER TABLE material_invoice DROP building_id');
    }
}
