<?php

namespace App\Service;

use App\Config;
use App\Entity\Material\Material;
use App\Entity\Material\MaterialInvoice;
use App\Entity\Material\MaterialOrder;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MaterialService
{
    /**
     * @var Material[]|null
     */
    private $materials = null;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @param HttpClientInterface $client
     * @param PeriodService $periodService
     */
    public function __construct(
        HttpClientInterface $client,
        PeriodService $periodService
    ) {
        $this->client = $client;
        $this->periodService = $periodService;
    }

    /**
     *
     */
    public function flushCache(): void
    {
        $this->materials = null;
    }

    /**
     * @return Material[]
     */
    public function getCurrentMaterials(): array
    {
        if ($this->materials === null) {
            $response = $this->client->request(
                'GET',
                Config::MARKET_URL . '/material/materials'
            );

            $data = json_decode($response->getContent(), true);

            $this->materials = [];
            foreach ($data as $dataItem) {
                $material = new Material();
                $material->setMaterialType($dataItem['material_type']);
                $material->setPrice($dataItem['price']);
                $material->setYear($this->periodService->getCurrentYear());

                $this->materials[] = $material;
            }
        }

        return $this->materials;
    }

    /**
     * @param MaterialOrder $materialOrder
     * @return MaterialInvoice
     */
    public function placeOrder(MaterialOrder $materialOrder): MaterialInvoice
    {
        $requestData = [
            'material_type' => $materialOrder->getMaterialType(),
            'material_qty' => $materialOrder->getMaterialQty()
        ];

        $response = $this->client->request(
            'POST',
            Config::MARKET_URL . '/material/place-order',
            ['json' => $requestData]
        );

        $responseData = json_decode($response->getContent(), true);

        $materialInvoice = new MaterialInvoice();
        $materialInvoice->setTotal($responseData['total']);
        $materialInvoice->setYear($this->periodService->getCurrentYear());

        return $materialInvoice;
    }
}
