<?php

namespace App\Service;

use App\Config;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PeriodService
{
    /**
     * @var int|null
     */
    private $currentYear = null;

    /**
     * @var PeriodHandlerInterface[]
     */
    private $handlers = null;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string[]
     */
    private $handlerClasses;

    /**
     * @param HttpClientInterface $client
     * @param Container $container
     * @param string[] $handlerClasses
     */
    public function __construct(
        HttpClientInterface $client,
        Container $container,
        array $handlerClasses
    ) {
        $this->client = $client;
        $this->container = $container;
        $this->handlerClasses = $handlerClasses;
    }

    /**
     *
     */
    public function setNextYear(): void
    {
        $this->client->request(
            'GET',
            Config::MARKET_URL . '/period/set-next-year'
        );

        $this->resetAppState();
    }

    /**
     *
     */
    public function resetYear(): void
    {
        $this->client->request(
            'GET',
            Config::MARKET_URL . '/period/reset-year'
        );

        $this->resetAppState();
    }

    /**
     * @return int
     */
    public function getCurrentYear(): int
    {
        if ($this->currentYear === null) {
            $response = $this->client->request(
                'GET',
                Config::MARKET_URL . '/period/year'
            );

            $this->currentYear = (int)$response->getContent();
        }

        return $this->currentYear;
    }

    /**
     *
     */
    private function resetAppState(): void
    {
        // Clean cache
        $this->currentYear = null;

        if ($this->handlers === null) {
            $this->handlers = [];
            foreach ($this->handlerClasses as $handlerClass) {
                $this->handlers[] = $this->container->get($handlerClass);
            }
        }

        foreach ($this->handlers as $handler) {
            $handler->reset();
        }
    }
}
