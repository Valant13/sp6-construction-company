<?php

namespace App\Service;

interface PeriodHandlerInterface
{
    function reset(): void;
}
