<?php

namespace App\Service;

use App\Entity\Apartment\Apartment;
use App\Entity\Apartment\ApartmentOrder;
use App\Entity\Apartment\Building;
use App\Entity\Apartment\BuildingProject;
use App\Entity\Apartment\BuildingType;
use App\Entity\Apartment\Demand;
use App\Entity\Lot\Lot;
use App\Entity\Lot\LotOrder;
use App\Entity\Material\Material;
use App\Entity\Material\MaterialOrder;
use App\Entity\Material\MaterialType;
use App\Repository\Apartment\ApartmentInvoiceRepository;
use App\Repository\Apartment\ApartmentRepository;
use App\Repository\Apartment\BuildingProjectRepository;
use App\Repository\Apartment\DemandRepository;
use App\Repository\Lot\LotInvoiceRepository;
use App\Repository\Material\MaterialInvoiceRepository;
use App\Repository\Material\MaterialRepository;
use Doctrine\ORM\EntityManagerInterface;

class ManufacturingService
{
    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @var ApartmentService
     */
    private $apartmentService;

    /**
     * @var LotService
     */
    private $lotService;

    /**
     * @var MaterialService
     */
    private $materialService;

    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var ApartmentInvoiceRepository
     */
    private $apartmentInvoiceRepository;

    /**
     * @var BuildingProjectRepository
     */
    private $buildingProjectRepository;

    /**
     * @var DemandRepository
     */
    private $demandRepository;

    /**
     * @var LotInvoiceRepository
     */
    private $lotInvoiceRepository;

    /**
     * @var MaterialRepository
     */
    private $materialRepository;

    /**
     * @var MaterialInvoiceRepository
     */
    private $materialInvoiceRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param PeriodService $periodService
     * @param ApartmentService $apartmentService
     * @param LotService $lotService
     * @param MaterialService $materialService
     * @param ApartmentRepository $apartmentRepository
     * @param ApartmentInvoiceRepository $apartmentInvoiceRepository
     * @param BuildingProjectRepository $buildingProjectRepository
     * @param DemandRepository $demandRepository
     * @param LotInvoiceRepository $lotInvoiceRepository
     * @param MaterialRepository $materialRepository
     * @param MaterialInvoiceRepository $materialInvoiceRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        PeriodService $periodService,
        ApartmentService $apartmentService,
        LotService $lotService,
        MaterialService $materialService,
        ApartmentRepository $apartmentRepository,
        ApartmentInvoiceRepository $apartmentInvoiceRepository,
        BuildingProjectRepository $buildingProjectRepository,
        DemandRepository $demandRepository,
        LotInvoiceRepository $lotInvoiceRepository,
        MaterialRepository $materialRepository,
        MaterialInvoiceRepository $materialInvoiceRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->periodService = $periodService;
        $this->apartmentService = $apartmentService;
        $this->lotService = $lotService;
        $this->materialService = $materialService;
        $this->apartmentRepository = $apartmentRepository;
        $this->apartmentInvoiceRepository = $apartmentInvoiceRepository;
        $this->buildingProjectRepository = $buildingProjectRepository;
        $this->demandRepository = $demandRepository;
        $this->lotInvoiceRepository = $lotInvoiceRepository;
        $this->materialRepository = $materialRepository;
        $this->materialInvoiceRepository = $materialInvoiceRepository;
        $this->entityManager = $entityManager;
    }

    /**
     *
     */
    public function initializeSession(): void
    {
        $this->periodService->resetYear();

        $lotInvoices = $this->lotInvoiceRepository->findAll();
        foreach ($lotInvoices as $lotInvoice) {
            $this->entityManager->remove($lotInvoice);
        }

        $materials = $this->materialRepository->findAll();
        foreach ($materials as $material) {
            $this->entityManager->remove($material);
        }

        $materialInvoices = $this->materialInvoiceRepository->findAll();
        foreach ($materialInvoices as $materialInvoice) {
            $this->entityManager->remove($materialInvoice);
        }

        $demandItems = $this->demandRepository->findAll();
        foreach ($demandItems as $demandItem) {
            $this->entityManager->remove($demandItem);
        }

        $apartmentInvoices = $this->apartmentInvoiceRepository->findAll();
        foreach ($apartmentInvoices as $apartmentInvoice) {
            $this->entityManager->remove($apartmentInvoice);
        }

        $buildingProjects = $this->buildingProjectRepository->findAll();
        foreach ($buildingProjects as $buildingProject) {
            $this->entityManager->remove($buildingProject);
        }

        $this->entityManager->flush();
    }

    /**
     *
     */
    public function startYear(): void
    {
        $demandItems = $this->apartmentService->getCurrentDemand();
        foreach ($demandItems as $demandItem) {
            $this->entityManager->persist($demandItem);
        }

        $materials = $this->materialService->getCurrentMaterials();
        foreach ($materials as $material) {
            $this->entityManager->persist($material);
        }

        $this->entityManager->flush();
    }

    /**
     * $map[$apartmentType][$buildingType] = $apartmentQty
     *
     * @return array
     */
    public function getDemand(): array
    {
        return $this->apartmentService->convertDemandToMap($this->apartmentService->getCurrentDemand());
    }

    /**
     * @return Material[]
     */
    public function getMaterials(): array
    {
        return $this->materialService->getCurrentMaterials();
    }

    /**
     * @return Lot[]
     */
    public function getLots(): array
    {
        return $this->lotService->getCurrentLots();
    }

    /**
     * @return BuildingProject[]
     */
    public function getBuildingProjects(): array
    {
        return $this->buildingProjectRepository->findAll();
    }

    /**
     * @param callable $buildingTypeCallback
     * @return BuildingProject
     */
    public function createBuildingProject(callable $buildingTypeCallback): BuildingProject
    {
        $buildingTypes = [
            BuildingType::HIGH_RISE,
            BuildingType::MID_RISE,
            BuildingType::LOW_RISE
        ];

        $buildingType = $buildingTypeCallback($buildingTypes);

        $buildingProject = $this->apartmentService->createBuildingProject($buildingType);

        $this->entityManager->persist($buildingProject);
        $this->entityManager->flush();

        return $buildingProject;
    }

    /**
     * @param callable $buildingProjectCallback
     * @param callable $lotCallback
     * @return Building
     */
    public function buildBuilding(callable $buildingProjectCallback, callable $lotCallback): Building
    {
        $buildingProjects = $this->buildingProjectRepository->findAll();

        /** @var BuildingProject $buildingProject */
        $buildingProject = $buildingProjectCallback($buildingProjects);

        $lots = $this->lotService->getCurrentLots();

        $acceptableLots = [];
        foreach ($lots as $lot) {
            if ($lot->getArea() >= $buildingProject->getMinLotArea()) {
                $acceptableLots[] = $lot;
            }
        }

        /** @var Lot $lot */
        $lot = $lotCallback($acceptableLots);

        $lotOrder = new LotOrder();
        $lotOrder->setLotId($lot->getId());

        $lotInvoice = $this->lotService->placeOrder($lotOrder);
        $this->entityManager->persist($lotInvoice);

        $materialTypes = [
            MaterialType::STEEL,
            MaterialType::CONCRETE,
            MaterialType::BRICKS
        ];

        $materialInvoices = [];
        foreach ($materialTypes as $materialType) {
            $materialQty = $buildingProject->getMaterialQty($materialType);

            $materialOrder = new MaterialOrder();
            $materialOrder->setMaterialType($materialType);
            $materialOrder->setMaterialQty($materialQty);

            $materialInvoice = $this->materialService->placeOrder($materialOrder);
            $this->entityManager->persist($materialInvoice);

            $materialInvoices[] = $materialInvoice;
        }

        $building = $this->apartmentService->createBuilding($buildingProject, $lotInvoice, $materialInvoices);
        $this->entityManager->persist($building);
        $this->entityManager->flush();

        return $building;
    }

    /**
     * @return Apartment[]
     */
    public function getUnsoldApartments(): array
    {
        return $this->apartmentRepository->findUnsold();
    }

    /**
     * @return ApartmentOrder[]
     */
    public function sellApartments(): array
    {
        $apartments = $this->apartmentRepository->findUnsold();

        $apartmentOrders = $this->apartmentService->offerApartments($apartments);
        $apartmentInvoices = $this->apartmentService->placeOrders($apartmentOrders);

        foreach ($apartmentInvoices as $apartmentInvoice) {
            $this->entityManager->persist($apartmentInvoice);
        }
        $this->entityManager->flush();

        return $apartmentOrders;
    }

    /**
     * @return int
     */
    public function finishYear(): int
    {
        $year = $this->periodService->getCurrentYear();

        $lotInvoices = $this->lotInvoiceRepository->findByYear($year);
        $materialInvoices = $this->materialInvoiceRepository->findByYear($year);
        $apartmentInvoices = $this->apartmentInvoiceRepository->findByYear($year);

        $profit = 0;

        foreach ($lotInvoices as $lotInvoice) {
            $profit -= $lotInvoice->getTotal();
        }

        foreach ($materialInvoices as $materialInvoice) {
            $profit -= $materialInvoice->getTotal();
        }

        foreach ($apartmentInvoices as $apartmentInvoice) {
            $profit += $apartmentInvoice->getTotal();
        }

        $this->periodService->setNextYear();

        return $profit;
    }
}
