<?php

namespace App\Service;

use App\Config;
use App\Entity\Lot\Lot;
use App\Entity\Lot\LotInvoice;
use App\Entity\Lot\LotOrder;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LotService
{
    /**
     * @var Lot[]|null
     */
    private $lots = null;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @param HttpClientInterface $client
     * @param PeriodService $periodService
     */
    public function __construct(
        HttpClientInterface $client,
        PeriodService $periodService
    ) {
        $this->client = $client;
        $this->periodService = $periodService;
    }

    /**
     *
     */
    public function flushCache(): void
    {
        $this->lots = null;
    }

    /**
     * @return Lot[]
     */
    public function getCurrentLots(): array
    {
        if ($this->lots === null) {
            $response = $this->client->request(
                'GET',
                Config::MARKET_URL . '/lot/lots'
            );

            $data = json_decode($response->getContent(), true);

            $this->lots = [];
            foreach ($data as $dataItem) {
                $lot = new Lot();
                $lot->setId($dataItem['id']);
                $lot->setArea($dataItem['area']);
                $lot->setPrice($dataItem['price']);

                $this->lots[$lot->getId()] = $lot;
            }
        }

        return $this->lots;
    }

    /**
     * @param LotOrder $lotOrder
     * @return LotInvoice
     */
    public function placeOrder(LotOrder $lotOrder): LotInvoice
    {
        $requestData = [
            'lot_id' => $lotOrder->getLotId()
        ];

        $response = $this->client->request(
            'POST',
            Config::MARKET_URL . '/lot/place-order',
            ['json' => $requestData]
        );

        $responseData = json_decode($response->getContent(), true);

        $lotInvoice = new LotInvoice();
        $lotInvoice->setTotal($responseData['total']);
        $lotInvoice->setYear($this->periodService->getCurrentYear());

        unset($this->lots[$lotOrder->getLotId()]);

        return $lotInvoice;
    }
}
