<?php

namespace App\Service;

use App\Config;
use App\Entity\Apartment\Apartment;
use App\Entity\Apartment\ApartmentInvoice;
use App\Entity\Apartment\ApartmentOrder;
use App\Entity\Apartment\ApartmentType;
use App\Entity\Apartment\Building;
use App\Entity\Apartment\BuildingProject;
use App\Entity\Apartment\BuildingType;
use App\Entity\Apartment\Customer;
use App\Entity\Apartment\Demand;
use App\Entity\Lot\LotInvoice;
use App\Entity\Material\MaterialInvoice;
use App\Repository\Apartment\ApartmentRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApartmentService
{
    /**
     * @var Demand[]|null
     */
    private $demandItems;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @param HttpClientInterface $client
     * @param PeriodService $periodService
     * @param ApartmentRepository $apartmentRepository
     */
    public function __construct(
        HttpClientInterface $client,
        PeriodService $periodService,
        ApartmentRepository $apartmentRepository
    ) {
        $this->client = $client;
        $this->periodService = $periodService;
        $this->apartmentRepository = $apartmentRepository;
    }

    /**
     *
     */
    public function flushCache(): void
    {
        $this->demandItems = null;
    }

    /**
     * @return Demand[]
     */
    public function getCurrentDemand(): array
    {
        if ($this->demandItems === null) {
            $response = $this->client->request(
                'GET',
                Config::MARKET_URL . '/apartment/demand'
            );

            $apartmentTypes = json_decode($response->getContent(), true);

            $this->demandItems = [];
            foreach ($apartmentTypes as $apartmentType => $buildingTypes) {
                foreach ($buildingTypes as $buildingType => $apartmentQty) {
                    $demandItem = new Demand();

                    $demandItem->setApartmentType($apartmentType);
                    $demandItem->setBuildingType($buildingType);
                    $demandItem->setApartmentQty($apartmentQty);
                    $demandItem->setYear($this->periodService->getCurrentYear());

                    $this->demandItems[] = $demandItem;
                }
            }
        }

        return $this->demandItems;
    }

    /**
     * @param string $buildingType
     * @return BuildingProject
     * @throws \Exception
     */
    public function createBuildingProject(string $buildingType): BuildingProject
    {
        $buildingProject = new BuildingProject();
        $buildingProject->setBuildingType($buildingType);
        $buildingProject->setOneBedroomQty($this->getRandomApartmentQty());
        $buildingProject->setTwoBedroomQty($this->getRandomApartmentQty());
        $buildingProject->setThreeBedroomQty($this->getRandomApartmentQty());
        $buildingProject->setSteelQty($this->getRandomMaterialQty());
        $buildingProject->setConcreteQty($this->getRandomMaterialQty());
        $buildingProject->setBricksQty($this->getRandomMaterialQty());
        $buildingProject->setMinLotArea($this->getRandomLotArea($buildingType));
        $buildingProject->setYear($this->periodService->getCurrentYear());

        return $buildingProject;
    }

    /**
     * @param Building $building
     * @return int
     */
    public function getBuildingCost(Building $building): int
    {
        $cost = 0;

        $cost += $building->getLotInvoice()->getTotal();
        foreach ($building->getMaterialInvoices() as $materialInvoice) {
            $cost += $materialInvoice->getTotal();
        }

        return $cost;
    }

    /**
     * @param BuildingProject $buildingProject
     * @param LotInvoice $lotInvoice
     * @param MaterialInvoice[] $materialInvoices
     * @return Building
     */
    public function createBuilding(
        BuildingProject $buildingProject,
        LotInvoice $lotInvoice,
        array $materialInvoices
    ): Building {
        $building = new Building();
        $building->setProject($buildingProject);
        $building->setYear($this->periodService->getCurrentYear());
        $building->setLotInvoice($lotInvoice);

        foreach ($materialInvoices as $materialInvoice) {
            $building->addMaterialInvoice($materialInvoice);
        }

        $apartmentTypes = [
            ApartmentType::ONE_BEDROOM,
            ApartmentType::TWO_BEDROOM,
            ApartmentType::THREE_BEDROOM
        ];

        $buildingArea = $this->getBuildingProjectArea($buildingProject);
        $buildingCost = $this->getBuildingCost($building);

        foreach ($apartmentTypes as $apartmentType) {
            $apartmentQty = $buildingProject->getApartmentQty($apartmentType);

            for ($i = 0; $i < $apartmentQty; $i++) {
                $building->addApartment($this->createApartment(
                    $apartmentType,
                    $building,
                    $buildingArea,
                    $buildingCost
                ));
            }
        }

        return $building;
    }

    /**
     * @param Apartment[] $apartments
     * @return ApartmentOrder[]
     */
    public function offerApartments(array $apartments): array
    {
        $requestData = [];
        foreach ($apartments as $apartment) {
            $requestData[] = [
                'id' => $apartment->getId(),
                'apartment_type' => $apartment->getApartmentType(),
                'building_type' => $apartment->getBuilding()->getProject()->getBuildingType(),
                'price' => $apartment->getPrice()
            ];
        }

        $response = $this->client->request(
            'POST',
            Config::MARKET_URL . '/apartment/offer-apartments',
            ['json' => $requestData]
        );

        $data = json_decode($response->getContent(), true);

        $orders = [];
        foreach ($data as $dataItem) {
            $customer = new Customer();
            $customer->setFirstName($dataItem['customer']['first_name']);
            $customer->setLastName($dataItem['customer']['last_name']);

            $order = new ApartmentOrder();
            $order->setCustomer($customer);
            $order->setApartmentId($dataItem['apartment_id']);

            $orders[] = $order;
        }

        return $orders;
    }

    /**
     * @param ApartmentOrder[] $apartmentOrders
     * @return ApartmentInvoice[]
     */
    public function placeOrders(array $apartmentOrders): array
    {
        $apartmentInvoices = [];
        $requestData = [];
        foreach ($apartmentOrders as $apartmentOrder) {
            $apartment = $this->apartmentRepository->find($apartmentOrder->getApartmentId());
            $customer = $apartmentOrder->getCustomer();

            $apartmentInvoice = new ApartmentInvoice();
            $apartmentInvoice->setApartment($apartment);
            $apartmentInvoice->setTotal($apartment->getPrice());
            $apartmentInvoice->setYear($this->periodService->getCurrentYear());
            $apartmentInvoice->setCustomerFirstName($customer->getFirstName());
            $apartmentInvoice->setCustomerLastName($customer->getLastName());

            $requestData[] = [
                'apartment_type' => $apartment->getApartmentType(),
                'building_type' => $apartment->getBuilding()->getProject()->getBuildingType()
            ];

            $apartmentInvoices[] = $apartmentInvoice;
        }

        $this->client->request(
            'POST',
            Config::MARKET_URL . '/apartment/apartment-invoices',
            ['json' => $requestData]
        );

        return $apartmentInvoices;
    }

    /**
     * $map[$apartmentType][$buildingType] = $apartmentQty
     *
     * @param Demand[] $demand
     * @return array
     */
    public function convertDemandToMap(array $demand): array
    {
        $map = [];

        foreach ($demand as $demandItem) {
            $apartmentType = $demandItem->getApartmentType();
            $buildingType = $demandItem->getBuildingType();

            if (!array_key_exists($apartmentType, $map)) {
                $map[$apartmentType] = [];
            }

            $map[$apartmentType][$buildingType] = $demandItem->getApartmentQty();
        }

        return $map;
    }

    /**
     * @return int
     */
    private function getRandomApartmentQty(): int
    {
        return rand(Config::MIN_BUILDING_APARTMENT_QTY, Config::MAX_BUILDING_APARTMENT_QTY);
    }

    /**
     * @return int
     */
    private function getRandomMaterialQty(): int
    {
        return rand(Config::MIN_BUILDING_MATERIAL_QTY, Config::MAX_BUILDING_MATERIAL_QTY);
    }

    /**
     * @param string $buildingType
     * @return int
     * @throws \Exception
     */
    private function getRandomLotArea(string $buildingType): int
    {
        switch ($buildingType) {
            case BuildingType::HIGH_RISE:
                return rand(Config::MIN_HIGH_RISE_BUILDING_LOT_AREA, Config::MAX_HIGH_RISE_BUILDING_LOT_AREA);
            case BuildingType::MID_RISE:
                return rand(Config::MIN_MID_RISE_BUILDING_LOT_AREA, Config::MAX_MID_RISE_BUILDING_LOT_AREA);
            case BuildingType::LOW_RISE:
                return rand(Config::MIN_LOW_RISE_BUILDING_LOT_AREA, Config::MAX_LOW_RISE_BUILDING_LOT_AREA);
            default:
                throw new \Exception("Building type $buildingType does not exist");
        }
    }

    /**
     * @param BuildingProject $buildingProject
     * @return int
     */
    private function getBuildingProjectArea(BuildingProject $buildingProject): int
    {
        return $buildingProject->getOneBedroomQty() * Config::ONE_BEDROOM_APARTMENT_AREA +
            $buildingProject->getTwoBedroomQty() * Config::TWO_BEDROOM_APARTMENT_AREA +
            $buildingProject->getThreeBedroomQty() * Config::THREE_BEDROOM_APARTMENT_AREA;
    }

    /**
     * @param string $apartmentType
     * @param Building $building
     * @param int $buildingArea
     * @param int $buildingCost
     * @return Apartment
     * @throws \Exception
     */
    private function createApartment(
        string $apartmentType,
        Building $building,
        int $buildingArea,
        int $buildingCost
    ): Apartment {
        switch ($apartmentType) {
            case ApartmentType::ONE_BEDROOM:
                $apartmentArea = Config::ONE_BEDROOM_APARTMENT_AREA;
                break;
            case ApartmentType::TWO_BEDROOM:
                $apartmentArea = Config::TWO_BEDROOM_APARTMENT_AREA;
                break;
            case ApartmentType::THREE_BEDROOM:
                $apartmentArea = Config::THREE_BEDROOM_APARTMENT_AREA;
                break;
            default:
                throw new \Exception("Apartment type $apartmentType does not exist");
        }

        $apartment = new Apartment();
        $apartment->setApartmentType($apartmentType);
        $apartment->setBuilding($building);
        $apartment->setArea($apartmentArea);
        $apartment->setPrice($buildingCost / $buildingArea * $apartmentArea * (1 + Config::APARTMENT_MARGIN));

        return $apartment;
    }
}
