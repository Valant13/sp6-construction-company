<?php

namespace App\Service\PeriodHandler;

use App\Service\ApartmentService;
use App\Service\PeriodHandlerInterface;

class ApartmentHandler implements PeriodHandlerInterface
{
    /**
     * @var ApartmentService
     */
    private $apartmentService;

    /**
     * @param ApartmentService $apartmentService
     */
    public function __construct(ApartmentService $apartmentService)
    {
        $this->apartmentService = $apartmentService;
    }

    public function reset(): void
    {
        $this->apartmentService->flushCache();
    }
}
