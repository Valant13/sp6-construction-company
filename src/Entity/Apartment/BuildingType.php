<?php

namespace App\Entity\Apartment;

class BuildingType
{
    const HIGH_RISE = 'high_rise';
    const MID_RISE = 'mid_rise';
    const LOW_RISE = 'low_rise';
}
