<?php

namespace App\Entity\Apartment;

use App\Entity\Material\MaterialType;
use App\Repository\Apartment\BuildingProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BuildingProjectRepository::class)
 */
class BuildingProject
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buildingType;

    /**
     * @ORM\Column(type="integer")
     */
    private $oneBedroomQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $twoBedroomQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $threeBedroomQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $steelQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $concreteQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $bricksQty;

    /**
     * @ORM\Column(type="integer")
     */
    private $minLotArea;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity=Building::class, mappedBy="project", orphanRemoval=true)
     */
    private $buildings;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getOneBedroomQty(): ?int
    {
        return $this->oneBedroomQty;
    }

    public function setOneBedroomQty(int $oneBedroomQty): self
    {
        $this->oneBedroomQty = $oneBedroomQty;

        return $this;
    }

    public function getTwoBedroomQty(): ?int
    {
        return $this->twoBedroomQty;
    }

    public function setTwoBedroomQty(int $twoBedroomQty): self
    {
        $this->twoBedroomQty = $twoBedroomQty;

        return $this;
    }

    public function getThreeBedroomQty(): ?int
    {
        return $this->threeBedroomQty;
    }

    public function setThreeBedroomQty(int $threeBedroomQty): self
    {
        $this->threeBedroomQty = $threeBedroomQty;

        return $this;
    }

    public function getSteelQty(): ?int
    {
        return $this->steelQty;
    }

    public function setSteelQty(int $steelQty): self
    {
        $this->steelQty = $steelQty;

        return $this;
    }

    public function getConcreteQty(): ?int
    {
        return $this->concreteQty;
    }

    public function setConcreteQty(int $concreteQty): self
    {
        $this->concreteQty = $concreteQty;

        return $this;
    }

    public function getBricksQty(): ?int
    {
        return $this->bricksQty;
    }

    public function setBricksQty(int $bricksQty): self
    {
        $this->bricksQty = $bricksQty;

        return $this;
    }

    public function getMinLotArea(): ?int
    {
        return $this->minLotArea;
    }

    public function setMinLotArea(int $minLotArea): self
    {
        $this->minLotArea = $minLotArea;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|Building[]
     */
    public function getBuildings(): Collection
    {
        return $this->buildings;
    }

    public function addBuilding(Building $building): self
    {
        if (!$this->buildings->contains($building)) {
            $this->buildings[] = $building;
            $building->setProject($this);
        }

        return $this;
    }

    public function removeBuilding(Building $building): self
    {
        if ($this->buildings->removeElement($building)) {
            // set the owning side to null (unless already changed)
            if ($building->getProject() === $this) {
                $building->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @param string $apartmentType
     * @return int
     * @throws \Exception
     */
    public function getApartmentQty(string $apartmentType): int
    {
        switch ($apartmentType) {
            case ApartmentType::ONE_BEDROOM:
                return $this->getOneBedroomQty();
            case ApartmentType::TWO_BEDROOM:
                return $this->getTwoBedroomQty();
            case ApartmentType::THREE_BEDROOM:
                return $this->getThreeBedroomQty();
            default:
                throw new \Exception("Apartment type $apartmentType does not exist");
        }
    }

    /**
     * @param string $materialType
     * @return int
     * @throws \Exception
     */
    public function getMaterialQty(string $materialType): int
    {
        switch ($materialType) {
            case MaterialType::CONCRETE:
                return $this->getConcreteQty();
            case MaterialType::STEEL:
                return $this->getSteelQty();
            case MaterialType::BRICKS:
                return $this->getBricksQty();
            default:
                throw new \Exception("Material type $materialType does not exist");
        }
    }
}
