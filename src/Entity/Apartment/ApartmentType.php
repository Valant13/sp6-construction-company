<?php

namespace App\Entity\Apartment;

class ApartmentType
{
    const ONE_BEDROOM = 'one_bedroom';
    const TWO_BEDROOM = 'two_bedroom';
    const THREE_BEDROOM = 'three_bedroom';
}
