<?php

namespace App\Entity\Apartment;

class ApartmentOrder
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var int
     */
    private $apartmentId;

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return int
     */
    public function getApartmentId(): int
    {
        return $this->apartmentId;
    }

    /**
     * @param int $apartmentId
     */
    public function setApartmentId(int $apartmentId): void
    {
        $this->apartmentId = $apartmentId;
    }
}
