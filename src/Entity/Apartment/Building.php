<?php

namespace App\Entity\Apartment;

use App\Entity\Lot\LotInvoice;
use App\Entity\Material\MaterialInvoice;
use App\Repository\Apartment\BuildingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BuildingRepository::class)
 */
class Building
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=BuildingProject::class, inversedBy="buildings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity=Apartment::class, mappedBy="building", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $apartments;

    /**
     * @ORM\OneToOne(targetEntity=LotInvoice::class, mappedBy="building", cascade={"persist", "remove"})
     */
    private $lotInvoice;

    /**
     * @ORM\OneToMany(targetEntity=MaterialInvoice::class, mappedBy="building")
     */
    private $materialInvoices;

    public function __construct()
    {
        $this->apartments = new ArrayCollection();
        $this->materialInvoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?BuildingProject
    {
        return $this->project;
    }

    public function setProject(?BuildingProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|Apartment[]
     */
    public function getApartments(): Collection
    {
        return $this->apartments;
    }

    public function addApartment(Apartment $apartment): self
    {
        if (!$this->apartments->contains($apartment)) {
            $this->apartments[] = $apartment;
            $apartment->setBuilding($this);
        }

        return $this;
    }

    public function removeApartment(Apartment $apartment): self
    {
        if ($this->apartments->removeElement($apartment)) {
            // set the owning side to null (unless already changed)
            if ($apartment->getBuilding() === $this) {
                $apartment->setBuilding(null);
            }
        }

        return $this;
    }

    public function getLotInvoice(): ?LotInvoice
    {
        return $this->lotInvoice;
    }

    public function setLotInvoice(?LotInvoice $lotInvoice): self
    {
        // unset the owning side of the relation if necessary
        if ($lotInvoice === null && $this->lotInvoice !== null) {
            $this->lotInvoice->setBuilding(null);
        }

        // set the owning side of the relation if necessary
        if ($lotInvoice !== null && $lotInvoice->getBuilding() !== $this) {
            $lotInvoice->setBuilding($this);
        }

        $this->lotInvoice = $lotInvoice;

        return $this;
    }

    /**
     * @return Collection|MaterialInvoice[]
     */
    public function getMaterialInvoices(): Collection
    {
        return $this->materialInvoices;
    }

    public function addMaterialInvoice(MaterialInvoice $materialInvoice): self
    {
        if (!$this->materialInvoices->contains($materialInvoice)) {
            $this->materialInvoices[] = $materialInvoice;
            $materialInvoice->setBuilding($this);
        }

        return $this;
    }

    public function removeMaterialInvoice(MaterialInvoice $materialInvoice): self
    {
        if ($this->materialInvoices->removeElement($materialInvoice)) {
            // set the owning side to null (unless already changed)
            if ($materialInvoice->getBuilding() === $this) {
                $materialInvoice->setBuilding(null);
            }
        }

        return $this;
    }
}
