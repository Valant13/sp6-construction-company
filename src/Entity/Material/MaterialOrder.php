<?php

namespace App\Entity\Material;

class MaterialOrder
{
    /**
     * @var string
     */
    private $materialType;

    /**
     * @var int
     */
    private $materialQty;

    /**
     * @return string
     */
    public function getMaterialType(): string
    {
        return $this->materialType;
    }

    /**
     * @param string $materialType
     */
    public function setMaterialType(string $materialType): void
    {
        $this->materialType = $materialType;
    }

    /**
     * @return int
     */
    public function getMaterialQty(): int
    {
        return $this->materialQty;
    }

    /**
     * @param int $materialQty
     */
    public function setMaterialQty(int $materialQty): void
    {
        $this->materialQty = $materialQty;
    }
}
