<?php

namespace App\Repository\Lot;

use App\Entity\Lot\LotInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LotInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method LotInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method LotInvoice[]    findAll()
 * @method LotInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LotInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LotInvoice::class);
    }

    /**
     * @param int $year
     * @return LotInvoice[]
     */
    public function findByYear(int $year)
    {
        return $this->createQueryBuilder('li')
            ->andWhere('li.year = :year')
            ->setParameter('year', $year)
            ->getQuery()
            ->getResult()
        ;
    }
}
