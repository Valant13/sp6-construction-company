<?php

namespace App\Repository\Apartment;

use App\Entity\Apartment\Apartment;
use App\Entity\Apartment\ApartmentInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\Join as JoinExpr;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Apartment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apartment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apartment[]    findAll()
 * @method Apartment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApartmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Apartment::class);
    }

    /**
     * @return Apartment[]
     */
    public function findUnsold()
    {
        return $this->createQueryBuilder('a')
            ->leftJoin(ApartmentInvoice::class, 'ai', JoinExpr::WITH, 'a = ai.apartment')
            ->andWhere('ai.id is NULL')
            ->getQuery()
            ->getResult()
        ;
    }
}
