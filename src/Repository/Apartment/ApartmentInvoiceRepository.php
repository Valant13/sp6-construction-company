<?php

namespace App\Repository\Apartment;

use App\Entity\Apartment\ApartmentInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApartmentInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApartmentInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApartmentInvoice[]    findAll()
 * @method ApartmentInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApartmentInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApartmentInvoice::class);
    }

    /**
     * @param int $year
     * @return ApartmentInvoice[]
     */
    public function findByYear(int $year)
    {
        return $this->createQueryBuilder('ai')
            ->andWhere('ai.year = :year')
            ->setParameter('year', $year)
            ->getQuery()
            ->getResult()
        ;
    }
}
