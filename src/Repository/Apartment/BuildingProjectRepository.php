<?php

namespace App\Repository\Apartment;

use App\Entity\Apartment\BuildingProject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BuildingProject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildingProject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildingProject[]    findAll()
 * @method BuildingProject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildingProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildingProject::class);
    }

    // /**
    //  * @return BuildingProject[] Returns an array of BuildingProject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BuildingProject
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
