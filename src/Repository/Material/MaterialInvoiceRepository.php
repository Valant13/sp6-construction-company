<?php

namespace App\Repository\Material;

use App\Entity\Material\MaterialInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MaterialInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialInvoice[]    findAll()
 * @method MaterialInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaterialInvoice::class);
    }

    /**
     * @param int $year
     * @return MaterialInvoice[]
     */
    public function findByYear(int $year)
    {
        return $this->createQueryBuilder('mi')
            ->andWhere('mi.year = :year')
            ->setParameter('year', $year)
            ->getQuery()
            ->getResult()
        ;
    }
}
