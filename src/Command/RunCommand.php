<?php

namespace App\Command;

use App\Config;
use App\Entity\Apartment\ApartmentType;
use App\Entity\Apartment\BuildingType;
use App\Entity\Material\MaterialType;
use App\Repository\Apartment\ApartmentRepository;
use App\Service\ManufacturingService;
use App\Service\PeriodService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class RunCommand extends Command
{
    protected static $defaultName = 'app:run';

    /**
     * @var ManufacturingService
     */
    private $manufacturingService;

    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @param ManufacturingService $manufacturingService
     * @param PeriodService $periodService
     * @param ApartmentRepository $apartmentRepository
     * @param string|null $name
     */
    public function __construct(
        ManufacturingService $manufacturingService,
        PeriodService $periodService,
        ApartmentRepository $apartmentRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->manufacturingService = $manufacturingService;
        $this->periodService = $periodService;
        $this->apartmentRepository = $apartmentRepository;
    }

    /**
     *
     */
    protected function configure(): void
    {
        $this->setDescription('Run application');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->writeln('Starting...');

        $this->manufacturingService->initializeSession();

        while (true) {
            $currentYear = $this->periodService->getCurrentYear();

            $helper = $this->getHelper('question');
            $yearQuestion = new ChoiceQuestion("<fg=green>Start new year (<fg=yellow>$currentYear</>)?</>", [
                'y' => '<fg=yellow>Yes</>',
                'a' => '<fg=yellow>Yes (autopilot)</>',
                'n' => '<fg=yellow>No (exit)</>'
            ], 'y');
            $io->newLine();
            $yearAnswer = $helper->ask($input, $output, $yearQuestion);

            if ($yearAnswer === 'y') {
                $this->runYearManually($input, $output);
            } elseif ($yearAnswer === 'a') {
                $this->runYearAutomatically($input, $output);
            } else {
                break;
            }
        }

        $io->writeln('Bye');

        return Command::SUCCESS;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function runYearManually(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');
        $continueQuestion = new ConfirmationQuestion('Press ENTER to continue...');
        $currentYear = $this->periodService->getCurrentYear();

        $this->manufacturingService->startYear();
        $io->writeln("$currentYear started");

        $demandQuestion = new ChoiceQuestion('<fg=green>Show demand for apartments?</>', [
            'y' => '<fg=yellow>Show demand</>',
            'n' => '<fg=yellow>No</>'
        ], 'y');
        $io->newLine();
        $demandAnswer = $helper->ask($input, $output, $demandQuestion);
        if ($demandAnswer === 'y') {
            $this->renderDemand($output);
            $helper->ask($input, $output, $continueQuestion);
        }

        $materialQuestion = new ChoiceQuestion('<fg=green>Show prices for materials?</>', [
            'y' => '<fg=yellow>Show materials</>',
            'n' => '<fg=yellow>No</>'
        ], 'y');
        $io->newLine();
        $materialAnswer = $helper->ask($input, $output, $materialQuestion);
        if ($materialAnswer === 'y') {
            $this->renderMaterials($output);
            $helper->ask($input, $output, $continueQuestion);
        }

        $lotQuestion = new ChoiceQuestion('<fg=green>Show available lots?</>', [
            'y' => '<fg=yellow>Show lots</>',
            'n' => '<fg=yellow>No</>'
        ], 'y');
        $io->newLine();
        $lotAnswer = $helper->ask($input, $output, $lotQuestion);
        if ($lotAnswer === 'y') {
            $this->renderLots($output);
            $helper->ask($input, $output, $continueQuestion);
        }

        while (true) {
            $projectQuestion = new ChoiceQuestion('<fg=green>Create new building project?</>', [
                'y' => '<fg=yellow>Create project</>',
                'n' => '<fg=yellow>No</>'
            ], 'y');
            $io->newLine();
            $projectAnswer = $helper->ask($input, $output, $projectQuestion);
            if ($projectAnswer === 'y') {
                $buildingProject = $this->manufacturingService->createBuildingProject(
                    function (array $buildingTypes) use ($helper, $input, $output, $io) {
                        $buildingTypeQuestion = new ChoiceQuestion('<fg=green>Select building type:</>', [
                            1 => '<fg=yellow>High-rise</>',
                            2 => '<fg=yellow>Middle-rise</>',
                            3 => '<fg=yellow>Low-rise</>'
                        ], 1);
                        $io->newLine();
                        $buildingTypeAnswer = $helper->ask($input, $output, $buildingTypeQuestion);
                        switch ($buildingTypeAnswer) {
                            case '<fg=yellow>High-rise</>':
                                return BuildingType::HIGH_RISE;
                            case '<fg=yellow>Middle-rise</>':
                                return BuildingType::MID_RISE;
                            default:
                                return BuildingType::LOW_RISE;
                        }
                    }
                );
                $io->writeln('New building project designed');

                $buildingTypeName = $this->getBuildingTypeName($buildingProject->getBuildingType());
                $oneBedroomQty = $buildingProject->getOneBedroomQty();
                $twoBedroomQty = $buildingProject->getTwoBedroomQty();
                $threeBedroomQty = $buildingProject->getThreeBedroomQty();
                $steelQty = $buildingProject->getSteelQty();
                $concreteQty = $buildingProject->getConcreteQty();
                $bricksQty = $buildingProject->getBricksQty();
                $minLotArea = $buildingProject->getMinLotArea();
                $designYear = $buildingProject->getYear();

                $io->writeln("<fg=green>Building type:</> $buildingTypeName");
                $io->writeln("<fg=green>Apartment quantity:</>");
                $io->writeln("  - <fg=green>One-bedroom apartments:</> $oneBedroomQty");
                $io->writeln("  - <fg=green>Two-bedroom apartments:</> $twoBedroomQty");
                $io->writeln("  - <fg=green>Three-bedroom apartments:</> $threeBedroomQty");
                $io->writeln("<fg=green>Building materials:</>");
                $io->writeln("  - <fg=green>Steel:</> $steelQty");
                $io->writeln("  - <fg=green>Concrete:</> $concreteQty");
                $io->writeln("  - <fg=green>Bricks:</> $bricksQty");
                $io->writeln("<fg=green>Min lot area for building:</> $minLotArea");
                $io->writeln("<fg=green>Year of design:</> $designYear");
                $io->newLine();

                $helper->ask($input, $output, $continueQuestion);
            } elseif ($projectAnswer === 'n') {
                $this->renderBuildingProjects($output);
                $helper->ask($input, $output, $continueQuestion);
                break;
            }
        }

        while (true) {
            $buildingQuestion = new ChoiceQuestion('<fg=green>Build new building?</>', [
                'y' => '<fg=yellow>Build building</>',
                'n' => '<fg=yellow>No</>'
            ], 'y');
            $io->newLine();
            $buildingAnswer = $helper->ask($input, $output, $buildingQuestion);
            if ($buildingAnswer === 'y') {
                $building = $this->manufacturingService->buildBuilding(
                    function (array $buildingProjects) use ($helper, $input, $output, $io) {
                        $indexedBuildingProjects = [];
                        $choices = [];
                        foreach ($buildingProjects as $buildingProject) {
                            $id = $buildingProject->getId();
                            $choices[$id] = "<fg=yellow>$id</>";
                            $indexedBuildingProjects["<fg=yellow>$id</>"] = $buildingProject;
                        }

                        $buildingProjectQuestion = new ChoiceQuestion(
                            '<fg=green>Select building project:</>',
                            $choices,
                            array_key_first($choices)
                        );
                        $io->newLine();
                        $buildingProjectAnswer = $helper->ask($input, $output, $buildingProjectQuestion);

                        return $indexedBuildingProjects[$buildingProjectAnswer];
                    },
                    function (array $lots) use ($helper, $input, $output, $io) {
                        $indexedLots = [];
                        $choices = [];
                        foreach ($lots as $lot) {
                            $id = $lot->getId();
                            $choices[$id] = "<fg=yellow>$id</>";
                            $indexedLots["<fg=yellow>$id</>"] = $lot;
                        }

                        $lotQuestion = new ChoiceQuestion(
                            '<fg=green>Select building project:</>',
                            $choices,
                            array_key_first($choices)
                        );
                        $io->newLine();
                        $lotAnswer = $helper->ask($input, $output, $lotQuestion);

                        return $indexedLots[$lotAnswer];
                    }
                );
                $apartments = $building->getApartments();
                $apartmentQty = count($apartments);
                $io->writeln("$apartmentQty new apartments built");

                $rows = [];
                $buildingTypeName = $this->getBuildingTypeName($building->getProject()->getBuildingType());
                foreach ($apartments as $apartment) {
                    $rows[] = [
                        $apartment->getId(),
                        $this->getApartmentTypeName($apartment->getApartmentType()),
                        $buildingTypeName,
                        $apartment->getArea(),
                        $apartment->getPrice()
                    ];
                }

                $table = new Table($output);
                $table->setHeaders([
                    'ID',
                    'Apartment type',
                    'Building type',
                    'Area',
                    'Price'
                ])->setRows($rows);
                $table->render();

                $helper->ask($input, $output, $continueQuestion);
            } elseif ($buildingAnswer === 'n') {
                break;
            }
        }

        $apartmentQuestion = new ChoiceQuestion('<fg=green>Show unsold apartments?</>', [
            'y' => '<fg=yellow>Show apartments</>',
            'n' => '<fg=yellow>No</>'
        ], 'y');
        $io->newLine();
        $apartmentAnswer = $helper->ask($input, $output, $apartmentQuestion);
        if ($apartmentAnswer === 'y') {
            $this->renderUnsoldApartments($output);
            $helper->ask($input, $output, $continueQuestion);
        }

        $offerQuestion = new ChoiceQuestion('<fg=green>Sell apartments?</>', [
            'y' => '<fg=yellow>Sell apartments</>',
            'n' => '<fg=yellow>No</>'
        ], 'y');
        $io->newLine();
        $offerAnswer = $helper->ask($input, $output, $offerQuestion);
        if ($offerAnswer === 'y') {
            $apartmentOrders = $this->manufacturingService->sellApartments();

            $rows = [];
            foreach ($apartmentOrders as $apartmentOrder) {
                $apartment = $this->apartmentRepository->find($apartmentOrder->getApartmentId());
                $rows[] = [
                    $apartmentOrder->getCustomer()->getFirstName(),
                    $apartmentOrder->getCustomer()->getLastName(),
                    $this->getApartmentTypeName($apartment->getApartmentType()),
                    $this->getBuildingTypeName($apartment->getBuilding()->getProject()->getBuildingType()),
                    $apartment->getBuilding()->getYear(),
                    $apartment->getPrice()
                ];
            }

            $output->writeln('Orders for apartments');

            $table = new Table($output);
            $table->setHeaders([
                'Customer first name',
                'Customer last name',
                'Apartment type',
                'Building type',
                'Building year',
                'Total'
            ])->setRows($rows);
            $table->render();
        }

        $yearQuestion = new ConfirmationQuestion("Press ENTER to finish year (<fg=yellow>$currentYear</>)");
        $helper->ask($input, $output, $yearQuestion);

        $profit = $this->manufacturingService->finishYear();
        $io->writeln("$currentYear finished");
        $io->writeln("<fg=green>Profit for the year:</> $profit");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function runYearAutomatically(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $currentYear = $this->periodService->getCurrentYear();

        $this->manufacturingService->startYear();
        $io->writeln("$currentYear started");
        $io->newLine();

        $buildingProjectQty = rand(Config::MIN_BUILDING_PROJECT_QTY, Config::MAX_BUILDING_PROJECT_QTY);
        for ($i = 0; $i < $buildingProjectQty; $i++) {
            $this->manufacturingService->createBuildingProject(
                function (array $buildingTypes) {
                    return $buildingTypes[array_rand($buildingTypes)];
                }
            );
            $io->writeln('New building project designed');
        }

        $buildingQty = rand(Config::MIN_BUILDING_QTY, Config::MAX_BUILDING_QTY);
        for ($i = 0; $i < $buildingQty; $i++) {
            $building = $this->manufacturingService->buildBuilding(
                function (array $buildingProjects) {
                    return $buildingProjects[array_rand($buildingProjects)];
                },
                function (array $lots) {
                    return $lots[array_rand($lots)];
                }
            );
            $apartments = $building->getApartments();
            $apartmentQty = count($apartments);
            $io->writeln("$apartmentQty new apartments built");
        }

        $apartmentOrders = $this->manufacturingService->sellApartments();
        $apartmentOrderQty = count($apartmentOrders);
        $output->writeln("$apartmentOrderQty apartments sold");

        $profit = $this->manufacturingService->finishYear();
        $io->newLine();
        $io->writeln("$currentYear finished");
        $io->writeln("<fg=green>Profit for the year:</> $profit");
    }

    /**
     * @param OutputInterface $output
     */
    private function renderDemand(OutputInterface $output)
    {
        $demand = $this->manufacturingService->getDemand();

        $currentYear = $this->periodService->getCurrentYear();
        $output->writeln("Demand for apartments in $currentYear");

        $table = new Table($output);
        $table->setHeaders(['', 'One-bedroom', 'Two-bedroom', 'Three-bedroom'])->setRows([
            ['<fg=green>High-rise</>', $demand['one_bedroom']['high_rise'], $demand['two_bedroom']['high_rise'], $demand['three_bedroom']['high_rise']],
            ['<fg=green>Mid-rise</>', $demand['one_bedroom']['mid_rise'], $demand['two_bedroom']['mid_rise'], $demand['three_bedroom']['mid_rise']],
            ['<fg=green>Low-rise</>', $demand['one_bedroom']['low_rise'], $demand['two_bedroom']['low_rise'], $demand['three_bedroom']['low_rise']]
        ]);
        $table->render();
    }

    /**
     * @param OutputInterface $output
     */
    private function renderMaterials(OutputInterface $output)
    {
        $materials = $this->manufacturingService->getMaterials();
        foreach ($materials as $material) {
            $rows[] = [
                $this->getMaterialTypeName($material->getMaterialType()),
                $material->getPrice()
            ];
        }

        $currentYear = $this->periodService->getCurrentYear();
        $output->writeln("Prices for materials in $currentYear");

        $table = new Table($output);
        $table->setHeaders(['Material', 'Price'])->setRows($rows);
        $table->render();
    }

    /**
     * @param OutputInterface $output
     */
    private function renderLots(OutputInterface $output)
    {
        $lots = $this->manufacturingService->getLots();

        $rows = [];
        foreach ($lots as $lot) {
            $rows[] = [
                $lot->getId(),
                $lot->getArea(),
                $lot->getPrice()
            ];
        }

        $currentYear = $this->periodService->getCurrentYear();
        $output->writeln("Available lots in $currentYear");

        $table = new Table($output);
        $table->setHeaders(['ID', 'Area', 'Price'])->setRows($rows);
        $table->render();
    }

    /**
     * @param OutputInterface $output
     */
    private function renderBuildingProjects(OutputInterface $output)
    {
        $buildingProjects = $this->manufacturingService->getBuildingProjects();

        $rows = [];
        foreach ($buildingProjects as $buildingProject) {
            $rows[] = [
                $buildingProject->getId(),
                $this->getBuildingTypeName($buildingProject->getBuildingType()),
                $buildingProject->getOneBedroomQty(),
                $buildingProject->getTwoBedroomQty(),
                $buildingProject->getThreeBedroomQty(),
                $buildingProject->getSteelQty(),
                $buildingProject->getConcreteQty(),
                $buildingProject->getBricksQty(),
                $buildingProject->getMinLotArea(),
                $buildingProject->getYear()
            ];
        }

        $output->writeln('Available building projects');

        $table = new Table($output);
        $table->setHeaders([
            'ID',
            'Type',
            'One-bedroom',
            'Two-bedroom',
            'Three-bedroom',
            'Steel',
            'Concrete',
            'Bricks',
            'Lot area',
            'Design year'
        ])->setRows($rows);
        $table->render();
    }

    /**
     * @param OutputInterface $output
     */
    private function renderUnsoldApartments(OutputInterface $output)
    {
        $apartments = $this->manufacturingService->getUnsoldApartments();

        $rows = [];
        foreach ($apartments as $apartment) {
            $rows[] = [
                $apartment->getId(),
                $this->getApartmentTypeName($apartment->getApartmentType()),
                $this->getBuildingTypeName($apartment->getBuilding()->getProject()->getBuildingType()),
                $apartment->getArea(),
                $apartment->getPrice(),
                $apartment->getBuilding()->getYear()
            ];
        }

        $output->writeln('Unsold apartments');

        $table = new Table($output);
        $table->setHeaders([
            'ID',
            'Apartment type',
            'Building type',
            'Area',
            'Price',
            'Building year'
        ])->setRows($rows);
        $table->render();
    }

    /**
     * @param string $apartmentType
     * @return string
     */
    private function getApartmentTypeName(string $apartmentType): string
    {
        switch ($apartmentType) {
            case ApartmentType::ONE_BEDROOM:
                return 'One-bedroom';
            case ApartmentType::TWO_BEDROOM:
                return 'Two-bedroom';
            default:
                return 'Three-bedroom';
        }
    }

    /**
     * @param string $buildingType
     * @return string
     */
    private function getBuildingTypeName(string $buildingType): string
    {
        switch ($buildingType) {
            case BuildingType::HIGH_RISE:
                return 'High-rise';
            case BuildingType::MID_RISE:
                return 'Mid-rise';
            default:
                return 'Low-rise';
        }
    }

    /**
     * @param string $materialType
     * @return string
     */
    private function getMaterialTypeName(string $materialType): string
    {
        switch ($materialType) {
            case MaterialType::STEEL:
                return 'Steel';
            case MaterialType::CONCRETE:
                return 'Concrete';
            default:
                return 'Bricks';
        }
    }
}
