<?php

namespace App\Controller;

use App\Repository\Material\MaterialInvoiceRepository;
use App\Repository\Material\MaterialRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaterialController extends AbstractController
{
    /**
     * @var MaterialRepository
     */
    private $materialRepository;

    /**
     * @var MaterialInvoiceRepository
     */
    private $materialInvoiceRepository;

    /**
     * @param MaterialRepository $materialRepository
     * @param MaterialInvoiceRepository $materialInvoiceRepository
     */
    public function __construct(
        MaterialRepository $materialRepository,
        MaterialInvoiceRepository $materialInvoiceRepository
    ) {
        $this->materialRepository = $materialRepository;
        $this->materialInvoiceRepository = $materialInvoiceRepository;
    }

    /**
     * @Route("/material/materials", methods="GET", name="material_materials")
     */
    public function materials(): Response
    {
        $materials = $this->materialRepository->findAll();

        $result = [];
        foreach ($materials as $material) {
            $result[] = [
                'id' => $material->getId(),
                'material_type' => $material->getMaterialType(),
                'price' => $material->getPrice(),
                'year' => $material->getYear()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/material/material-invoices", methods="GET", name="material_material_invoices")
     */
    public function materialInvoices(): Response
    {
        $materialInvoices = $this->materialInvoiceRepository->findAll();

        $result = [];
        foreach ($materialInvoices as $materialInvoice) {
            $result[] = [
                'id' => $materialInvoice->getId(),
                'building_id' => $materialInvoice->getBuilding()->getId(),
                'total' => $materialInvoice->getTotal(),
                'year' => $materialInvoice->getYear()
            ];
        }

        return $this->json($result);
    }
}
