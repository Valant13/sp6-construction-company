<?php

namespace App\Controller;

use App\Repository\Apartment\ApartmentInvoiceRepository;
use App\Repository\Apartment\ApartmentRepository;
use App\Repository\Apartment\BuildingProjectRepository;
use App\Repository\Apartment\BuildingRepository;
use App\Repository\Apartment\DemandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApartmentController extends AbstractController
{
    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var ApartmentInvoiceRepository
     */
    private $apartmentInvoiceRepository;

    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * @var BuildingProjectRepository
     */
    private $buildingProjectRepository;

    /**
     * @var DemandRepository
     */
    private $demandRepository;

    /**
     * @param ApartmentRepository $apartmentRepository
     * @param ApartmentInvoiceRepository $apartmentInvoiceRepository
     * @param BuildingRepository $buildingRepository
     * @param BuildingProjectRepository $buildingProjectRepository
     * @param DemandRepository $demandRepository
     */
    public function __construct(
        ApartmentRepository $apartmentRepository,
        ApartmentInvoiceRepository $apartmentInvoiceRepository,
        BuildingRepository $buildingRepository,
        BuildingProjectRepository $buildingProjectRepository,
        DemandRepository $demandRepository
    ) {
        $this->apartmentRepository = $apartmentRepository;
        $this->apartmentInvoiceRepository = $apartmentInvoiceRepository;
        $this->buildingRepository = $buildingRepository;
        $this->buildingProjectRepository = $buildingProjectRepository;
        $this->demandRepository = $demandRepository;
    }

    /**
     * @Route("/apartment/apartments", methods="GET", name="apartment_apartments")
     */
    public function apartments(): Response
    {
        $apartments = $this->apartmentRepository->findAll();

        $result = [];
        foreach ($apartments as $apartment) {
            $result[] = [
                'id' => $apartment->getId(),
                'apartment_type' => $apartment->getApartmentType(),
                'building_id' => $apartment->getBuilding()->getId(),
                'area' => $apartment->getArea(),
                'price' => $apartment->getPrice()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/apartment/apartment-invoices", methods="GET", name="apartment_apartment_invoices")
     */
    public function apartmentInvoices(): Response
    {
        $apartmentInvoices = $this->apartmentInvoiceRepository->findAll();

        $result = [];
        foreach ($apartmentInvoices as $apartmentInvoice) {
            $result[] = [
                'id' => $apartmentInvoice->getId(),
                'apartment_id' => $apartmentInvoice->getApartment()->getId(),
                'total' => $apartmentInvoice->getTotal(),
                'year' => $apartmentInvoice->getYear(),
                'customer_first_name' => $apartmentInvoice->getCustomerFirstName(),
                'customer_last_name' => $apartmentInvoice->getCustomerLastName()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/apartment/buildings", methods="GET", name="apartment_buildings")
     */
    public function buildings(): Response
    {
        $buildings = $this->buildingRepository->findAll();

        $result = [];
        foreach ($buildings as $building) {
            $result[] = [
                'id' => $building->getId(),
                'project_id' => $building->getProject()->getId(),
                'year' => $building->getYear()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/apartment/building-projects", methods="GET", name="apartment_building_projects")
     */
    public function buildingProjects(): Response
    {
        $buildingProjects = $this->buildingProjectRepository->findAll();

        $result = [];
        foreach ($buildingProjects as $buildingProject) {
            $result[] = [
                'id' => $buildingProject->getId(),
                'building_type' => $buildingProject->getBuildingType(),
                'one_bedroom_qty' => $buildingProject->getOneBedroomQty(),
                'two_bedroom_qty' => $buildingProject->getTwoBedroomQty(),
                'three_bedroom_qty' => $buildingProject->getThreeBedroomQty(),
                'steel_qty' => $buildingProject->getSteelQty(),
                'concrete_qty' => $buildingProject->getConcreteQty(),
                'bricks_qty' => $buildingProject->getBricksQty(),
                'min_lot_area' => $buildingProject->getMinLotArea(),
                'year' => $buildingProject->getYear()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/apartment/demands", methods="GET", name="apartment_demands")
     */
    public function demands(): Response
    {
        $demands = $this->demandRepository->findAll();

        $result = [];
        foreach ($demands as $demand) {
            $result[] = [
                'id' => $demand->getId(),
                'apartment_type' => $demand->getApartmentType(),
                'building_type' => $demand->getBuildingType(),
                'apartment_qty' => $demand->getApartmentQty(),
                'year' => $demand->getYear()
            ];
        }

        return $this->json($result);
    }
}
