<?php

namespace App\Controller;

use App\Repository\Lot\LotInvoiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LotController extends AbstractController
{
    /**
     * @var LotInvoiceRepository
     */
    private $lotInvoiceRepository;

    /**
     * @param LotInvoiceRepository $lotInvoiceRepository
     */
    public function __construct(
        LotInvoiceRepository $lotInvoiceRepository
    ) {
        $this->lotInvoiceRepository = $lotInvoiceRepository;
    }

    /**
     * @Route("/lot/lot-invoices", methods="GET", name="lot_lot_invoices")
     */
    public function lotInvoices(): Response
    {
        $lotInvoices = $this->lotInvoiceRepository->findAll();

        $result = [];
        foreach ($lotInvoices as $lotInvoice) {
            $result[] = [
                'id' => $lotInvoice->getId(),
                'building_id' => $lotInvoice->getBuilding()->getId(),
                'total' => $lotInvoice->getTotal(),
                'year' => $lotInvoice->getYear()
            ];
        }

        return $this->json($result);
    }
}
